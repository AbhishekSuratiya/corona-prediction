from tkinter import *
sys.setrecursionlimit(5999)

root=Tk()
root.geometry("1500x800")
root.title("COVID-19 Analysis")
root.minsize(1500,800)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import random
import math
import time
from sklearn.model_selection import RandomizedSearchCV, train_test_split
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error, mean_absolute_error
import datetime
import operator
plt.style.use('seaborn')

# Loading datasets
confirmed_cases = pd.read_csv('C:/Users/Abhishek/Desktop/Tkinter/time_series_covid-19_confirmed.csv')
deaths_reported = pd.read_csv('C:/Users/Abhishek/Desktop/Tkinter/time_series_covid-19_deaths.csv')
recovered_cases = pd.read_csv('C:/Users/Abhishek/Desktop/Tkinter/time_series_covid-19_recovered.csv')
cols = confirmed_cases.keys()
confirmed = confirmed_cases.loc[:, cols[4]:cols[-1]]
deaths = deaths_reported.loc[:, cols[4]:cols[-1]]
recoveries = recovered_cases.loc[:, cols[4]:cols[-1]]
# Finding the total confirmed cases, death cases and the recovered cases and append them to an 4 empty lists
# Also, calculate the total mortality rate which is the death_sum/confirmed cases

dates = confirmed.keys()
world_cases = []
total_deaths = []
mortality_rate = []
total_recovered = []

for i in dates:
    confirmed_sum = confirmed[i].sum()
    death_sum = deaths[i].sum()
    recovered_sum = recoveries[i].sum()
    world_cases.append(confirmed_sum)
    total_deaths.append(death_sum)
    mortality_rate.append(death_sum/confirmed_sum)
    total_recovered.append(recovered_sum)

logo=PhotoImage(file="corona.png")
Label(image=logo).grid(row=0,column=0)

msg=PhotoImage(file="msg.png")
Label(image=msg).grid(row=10,column=1,rowspan=7, sticky=N+W+E+S)

Label(text="COVID-19 PREDICTION & ANALYSIS",font="times 24 underline",bg="Grey",fg="white").grid(row=0, columnspan=4, pady=5)
Label(text="Confirmed Cases" ,font="arial 12 bold " ,bg='#034f84',fg="white", width=30).grid(row=1, column=0, sticky=W+E+N+S)
Label(text="Recovered Cases",font="arial 12 bold ",bg='#034f84',fg="white", width=30).grid(row=2, column=0, sticky=W+E+N+S)

Label(text="Death Cases",font="arial 12 bold ",bg='#034f84',fg="white", width=30).grid(row=3, column=0, sticky=W+E+N+S)
Label(text="Areawise analysis",font="arial 14 bold ",bg='#bc5a45',fg="white", width=24).grid(row=1,column=2)
Label(text=confirmed_sum,font="arial 12 bold ",bg='#034f84',fg="white", width=30).grid(row=1, column=1, sticky=W+E+N+S)
Label(text=recovered_sum,font="arial 12 bold ",bg='#034f84',fg="white", width=30).grid(row=2, column=1, sticky=W+E+N+S)

Label(text=death_sum,font="arial 12 bold ",bg='#034f84',fg="white", width=30).grid(row=3, column=1, sticky=W+E+N+S)
Label(text="Graphs",font="arial 12 bold ",bg='#6b5b95',fg="white", width=31).grid(row=5,padx=1, column=0)
Label(text="Analysis By Graphs",font="arial 12 bold ",bg='#b1cbbb', width=33).grid(row=9,padx=1, column=0)
Label(text="Prediction By Linear Regression",font="arial 12 bold ",bg='#563f46',fg="white", width=31).grid(row=5,padx=2, column=1)
Label(text="Prediction By SVM",font="arial 12 bold ",bg='#563f46',fg="white", width=31).grid(row=5,padx=2, column=2)

# Convert all the dates and the cases in the form of a numpy array
days_since_1_22 = np.array([i for i in range(len(dates))]).reshape(-1, 1)
world_cases = np.array(world_cases).reshape(-1, 1)
total_deaths = np.array(total_deaths).reshape(-1, 1)
total_recovered = np.array(total_recovered).reshape(-1, 1)

# Future forecasting for the next 10 days
days_in_future = 10
future_forecast = np.array([i for i in range(len(dates)+days_in_future)]).reshape(-1, 1)
adjusted_dates = future_forecast[:-10]

# Convert all the integers into datetime for better visualization
start = '1/22/2020'
start_date = datetime.datetime.strptime(start, '%m/%d/%Y')
future_forecast_dates = []
for i in range(len(future_forecast)):
    future_forecast_dates.append((start_date + datetime.timedelta(days=i)).strftime('%m/%d/%Y'))

# For visualization with the latest data of 15th of march
latest_confirmed = confirmed_cases[dates[-1]]
latest_deaths = deaths_reported[dates[-1]]
latest_recoveries = recovered_cases[dates[-1]]

# Find the list of unique countries
unique_countries =  list(confirmed_cases['Country/Region'].unique())

# The next line of code will basically calculate the total number of confirmed cases by each country
country_confirmed_cases = []
no_cases = []
for i in unique_countries:
    cases = latest_confirmed[confirmed_cases['Country/Region'] == i].sum()
    if cases > 0:
        country_confirmed_cases.append(cases)
    else:
        no_cases.append(i)
for i in no_cases:
    unique_countries.remove(i)
unique_countries = [k for k, v in
                    sorted(zip(unique_countries, country_confirmed_cases), key=operator.itemgetter(1), reverse=True)]
for i in range(len(unique_countries)):
    country_confirmed_cases[i] = latest_confirmed[confirmed_cases['Country/Region'] == unique_countries[i]].sum()

def country_wise():
    txt.delete(0.0,'end')
    txt.insert('end','Confirmed Cases by Countries/Regions:\n')
    for i in range(len(unique_countries)):
        txt.insert('end',f'{unique_countries[i]}: {country_confirmed_cases[i]} cases\n')

Button(text="Confirmed Cases Country wise", command=country_wise,width=30, bg='#f18973', fg="white", font="arial 10",borderwidth=5,relief=RAISED).grid(row=2,column=2)

# Find the list of unique provinces
unique_provinces =  list(confirmed_cases['Province/State'].unique())

# Finding the number of confirmed cases per province, state or city
province_confirmed_cases = []
no_cases = []
for i in unique_provinces:
    cases = latest_confirmed[confirmed_cases['Province/State'] == i].sum()
    if cases > 0:
        province_confirmed_cases.append(cases)
    else:
        no_cases.append(i)
for i in no_cases:
    unique_provinces.remove(i)
def state_wise():
    txt.delete(0.0,'end')
    for i in range(len(unique_provinces)):
        txt.insert('end',f'{unique_provinces[i]}: {province_confirmed_cases[i]} cases\n')

Button(text="Confirmed Cases State wise",borderwidth=5,relief=RAISED, command=state_wise,width=30,font="arial 10", bg='#f18973', fg="white" ).grid(row=3,column=2)

# handling nan values if there is any
nan_indices = []
for i in range(len(unique_provinces)):
    if type(unique_provinces[i]) == float:
        nan_indices.append(i)
unique_provinces = list(unique_provinces)
province_confirmed_cases = list(province_confirmed_cases)
for i in nan_indices:
    unique_provinces.pop(i)
    province_confirmed_cases.pop(i)

def g_confirmed_countrywise():
    plt.figure(figsize=(64, 64))
    plt.barh(unique_countries, country_confirmed_cases)
    plt.title('Number of Covid-19 Confirmed Cases in Countries',size=1)
    plt.xlabel('Number of Covid19 Confirmed Cases')
    plt.show()

Button(text="Country Wise Cases by Graph", borderwidth=5,relief=RAISED,command=g_confirmed_countrywise,width=40,font="arial 10", bg="#deeaee").grid(row=11, column=0)

# Plot a bar graph to see the total confirmed cases between mainland china and outside mainland china
china_confirmed = latest_confirmed[confirmed_cases['Country/Region']=='China'].sum()
outside_mainland_china_confirmed = np.sum(country_confirmed_cases) - china_confirmed

def g_china_other():
    txt.delete(0.0,'end')
    txt.insert('end','Outside Mainland China: {} cases\n'.format(outside_mainland_china_confirmed))
    txt.insert('end','Mainland China: {} cases\n'.format(china_confirmed))
    txt.insert('end','Total: {} cases\n'.format(china_confirmed + outside_mainland_china_confirmed))
    plt.figure(figsize=(16, 9))
    plt.barh('Mainland China', china_confirmed)
    plt.barh('Outside Mainland China', outside_mainland_china_confirmed)
    plt.title('Number of Confirmed Coronavirus Cases')
    plt.show()

Button(text="Graph Between Mainland China and outside China",borderwidth=5,relief=RAISED,command=g_china_other,width=40,font="arial 10", bg="#deeaee").grid(row=10,column=0)

# Only show 10 countries with the most confirmed cases, the rest are grouped into the category named others
visual_unique_countries = []
visual_confirmed_cases = []
others = np.sum(country_confirmed_cases[10:])
for i in range(len(country_confirmed_cases[:10])):
    visual_unique_countries.append(unique_countries[i])
    visual_confirmed_cases.append(country_confirmed_cases[i])
visual_unique_countries.append('Others')
visual_confirmed_cases.append#034f84)

def g_top10_countries():
    plt.figure(figsize=(32, 18))
    plt.barh(visual_unique_countries, visual_confirmed_cases)
    plt.title('Number of Covid-19 Confirmed Cases in Countries/Regions', size=20)
    plt.show()

Button(text="Graph of 10 most affected Countries",borderwidth=5,relief=RAISED,command=g_top10_countries,width=40,font="arial 10", bg="#deeaee").grid(row=14, column=0)


# Create a pie chart to see the total confirmed cases in 10 different countries
c = random.choices(list(mcolors.CSS4_COLORS.values()),k = len(unique_countries))
def p_top10_countries():
    plt.figure(figsize=(20,20))
    plt.title('Covid-19 Confirmed Cases per Country')
    plt.pie(visual_confirmed_cases, colors = ['yellowgreen', 'gold','brown','blue','purple', 'lightskyblue', 'red', 'green', 'black', 'grey', 'lightcoral'])
    plt.legend(visual_unique_countries, loc='best')
    plt.show()

Button(text="Pie Chart of 10 most affected Countries",borderwidth=5,relief=RAISED,command=p_top10_countries,width=40,font="arial 10", bg="#deeaee").grid(row=16, column=0)

# # SVM model
# kernel = ['poly', 'sigmoid', 'rbf']
# c = [0.01, 0.1, 1, 10]
# gamma = [0.01, 0.1, 1]
# epsilon = [0.01, 0.1, 1]
# shrinking = [True, False]
# svm_grid = {'kernel': kernel, 'C': c, 'gamma' : gamma, 'epsilon': epsilon, 'shrinking' : shrinking}
# svm = SVR()
# svm_search = RandomizedSearchCV(svm, svm_grid, scoring='neg_mean_squared_error', cv=3, return_train_score=True, n_jobs=-1, n_iter=40, verbose=1)
# svm_search.fit(days_since_1_22, world_cases.ravel())
# svm_confirmed = svm_search.best_estimator_
# svm_pred = svm_confirmed.predict(future_forecast)
#
# # check against testing data
# svm_test_pred = svm_confirmed.predict(days_since_1_22)
# def testing_svm():
#     txt.delete(0.0,'end')
#     sm_a = mean_absolute_error(svm_test_pred, world_cases)
#     sm_s = mean_squared_error(svm_test_pred, world_cases)
#     txt.insert('end', 'MAE:' + str(sm_a) + '\n')
#     txt.insert('end', 'MSE:' + str(sm_s))
#     plt.figure(figsize=(20, 12))
#     plt.plot(svm_test_pred)
#     plt.plot(world_cases)
#     plt.show()
#
#
# Button(text="Testing Using SVM",bg="#c8c3cc", borderwidth=5,relief=RAISED,command=testing_svm,width=25,font="arial 10").grid(row=7, column=2)
# # Confirmed vs Predicted cases
#
# def g_pred_svm():
#     plt.figure(figsize=(20, 12))
#     plt.plot(adjusted_dates, world_cases)
#     plt.plot(future_forecast, svm_pred, linestyle='dashed', color='purple')
#     plt.title('Number of Coronavirus Cases Over Time', size=30)
#     plt.xlabel('Days Since 1/22/2020', size=30)
#     plt.ylabel('Number of Cases', size=30)
#     plt.legend(['Confirmed Cases', 'SVM predictions'])
#     plt.xticks(size=15)
#     plt.yticks(size=15)
#     plt.show()
#
# Button(text="Graphical Prediction", bg="#c8c3cc",borderwidth=5,relief=RAISED, command=g_pred_svm,width=25,font="arial 10").grid(row=8, column=2)
#
# def svm_10days():
#     txt.delete(0.0,'end')
#     # Predictions for the next 10 days using SVM
#     txt.insert('end','SVM future predictions:\n')
#     txt.insert('end',svm_pred[-10:].reshape(10,1))
#
# Button(text="Next 10 Days Prediction",bg="#c8c3cc",borderwidth=5,relief=RAISED, command=svm_10days,width=25,font="arial 10").grid(row=9, column=2)

def g_coronacases():
# Total Number of coronavirus cases over time
    plt.figure(figsize=(20, 12))
    plt.plot(adjusted_dates, world_cases)
    plt.title('Number of Coronavirus Cases Over Time', size=30)
    plt.xlabel('Days Since 1/22/2020', size=30)
    plt.ylabel('Number of Cases', size=30)
    plt.xticks(size=15)
    plt.yticks(size=15)
    plt.show()

Button(text="Confirmed Cases Graph",command=g_coronacases,borderwidth=5,relief=RAISED,width=20,font="arial 10",bg="#a2b9bc").grid(row=7, column=0)

# Using Linear regression model to make predictions
from sklearn.linear_model import LinearRegression
linear_model = LinearRegression(normalize=True, fit_intercept=True)
linear_model.fit(days_since_1_22, world_cases)
test_linear_pred = linear_model.predict(days_since_1_22)
linear_pred = linear_model.predict(future_forecast)

def test_linear():
    txt.delete(0.0,'end')
    m_a=mean_absolute_error(test_linear_pred, world_cases)
    m_s=mean_squared_error(test_linear_pred, world_cases)
    txt.insert('end','MAE:'+ str(m_a)+'\n')
    txt.insert('end','MSE:'+ str(m_s))
    plt.figure(figsize=(20, 12))
    plt.plot(adjusted_dates,world_cases)
    plt.plot(adjusted_dates,test_linear_pred)
    plt.title('Testing using Linear Regression', size=30)
    plt.xlabel('Days Since 1/22/2020', size=30)
    plt.ylabel('Number of Cases', size=30)
    plt.show()

Button(text="Testing using Linear Regression",command=test_linear,borderwidth=5,relief=RAISED,width=25,font="arial 10",bg="#c8c3cc").grid(row=7, column=1)

def prediction_linear():
    plt.figure(figsize=(20, 12))
    plt.plot(adjusted_dates, world_cases)
    plt.plot(future_forecast, linear_pred, linestyle='dashed', color='orange')
    plt.title('Number of Coronavirus Cases Over Time', size=30)
    plt.xlabel('Days Since 1/22/2020', size=30)
    plt.ylabel('Number of Cases', size=30)
    plt.legend(['Confirmed Cases', 'Linear Regression Predictions'])
    plt.xticks(size=15)
    plt.yticks(size=15)
    plt.show()

Button(text="Graphical Prediction",command=prediction_linear,borderwidth=5,relief=RAISED,width=25,font="arial 10",bg="#c8c3cc").grid(row=8, column=1)

def prediction_days_linear():
    # Predictions for the next 10 days using Linear Regression
    txt.delete(0.0,'end')
    txt.insert('end','Linear regression future predictions:\n')
    txt.insert('end',linear_pred[-10:])

Button(text="Next 10 days Prediction",command=prediction_days_linear,borderwidth=5,relief=RAISED,width=25,font="arial 10", bg="#c8c3cc").grid(row=9, column=1)

def g_death():
    # Total deaths over time
    plt.figure(figsize=(20, 12))
    plt.plot(adjusted_dates, total_deaths, color='red')
    plt.title('Number of Coronavirus Deaths Over Time', size=30)
    plt.xlabel('Time', size=30)
    plt.ylabel('Number of Deaths', size=30)
    plt.xticks(size=15)
    plt.yticks(size=15)
    plt.show()

Button(text="Death Cases Graph",command=g_death, width=20,borderwidth=5,relief=RAISED, font="arial 10", bg="#a2b9bc").grid(row=8, column=0)

mean_mortality_rate = np.mean(mortality_rate)
def g_mortality_rate():
    plt.figure(figsize=(20, 12))
    plt.plot(adjusted_dates, mortality_rate, color='orange')
    plt.axhline(y = mean_mortality_rate,linestyle='--', color='black')
    plt.title('Mortality Rate of Coronavirus Over Time', size=30)
    plt.legend(['mortality rate', 'y='+str(mean_mortality_rate)])
    plt.xlabel('Time', size=30)
    plt.ylabel('Mortality Rate', size=30)
    plt.xticks(size=15)
    plt.yticks(size=15)
    plt.show()

Button(text="Mortality Rate Graph",command=g_mortality_rate,borderwidth=5,relief=RAISED,width=40,font="arial 10", bg="#deeaee").grid(row=15, column=0)

def g_recovered():
    # Coronavirus Cases Recovered Over Time
    plt.figure(figsize=(20, 12))
    plt.plot(adjusted_dates, total_recovered, color='green')
    plt.title('Number of Coronavirus Cases Recovered Over Time', size=30)
    plt.xlabel('Time', size=30)
    plt.ylabel('Number of Cases', size=30)
    plt.xticks(size=15)
    plt.yticks(size=15)
    plt.show()

Button(text="Recovered Cases Graph",command=g_recovered,width=40,borderwidth=5,relief=RAISED,font="arial 10", bg="#deeaee").grid(row=12, column=0)

def g_rec_vs_death():
    # Number of Coronavirus cases recovered vs the number of deaths
    plt.figure(figsize=(20, 12))
    plt.plot(adjusted_dates, total_deaths, color='r')
    plt.plot(adjusted_dates, total_recovered, color='green')
    plt.legend(['deaths', 'recoveries'], loc='best', fontsize=20)
    plt.title('Number of Coronavirus Cases', size=30)
    plt.xlabel('Time', size=30)
    plt.ylabel('Number of Cases', size=30)
    plt.xticks(size=15)
    plt.yticks(size=15)
    plt.show()

Button(text="Recovered v/s Death Cases Graph",command=g_rec_vs_death,borderwidth=5,relief=RAISED,width=40,font="arial 10", bg="#deeaee").grid(row=13, column=0)
Button(text="Exit",command=quit,borderwidth=5,relief=RIDGE,width=8,height=5, font="arial 20", bg="#deeaee").grid(row=12, column=2, rowspan=2)

txt=Text(root, width=100, height=40,wrap=WORD,font="courier 20", pady=2)
txt.grid(row=0, column=5, rowspan=50,columnspan=5)
root.mainloop()